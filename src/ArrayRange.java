import java.util.Scanner;

public class ArrayRange {
    public static void main(String[] args){
        int n;
        Scanner sc = new Scanner(System.in);
        n=sc.nextInt();
        int mini=Integer.MAX_VALUE;
        int maxi = Integer.MIN_VALUE;
        int[] arr = new int[n];
        for(int i=0;i<n;i++){
            arr[i]=sc.nextInt();
            if(arr[i]>maxi)
                maxi=arr[i];
            if(arr[i]<mini)
                mini=arr[i];
        }
        System.out.println((maxi-mini));
    }
}
